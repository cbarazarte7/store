import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { combineReducers, createStore, compose, applyMiddleware } from 'redux';
import cartReducer from './ducks/cart';
import productsReducer from './ducks/products';
import App from './App';
import productsData from './data/products';
import 'bootstrap/dist/css/bootstrap.css';

const rootReducer = combineReducers({
    cart: cartReducer,
    products: productsReducer
});

var url = 'http://localhost:3000/movies.json'

fetch(url)
.then(function(response) {
  return response.json()
})
.then(function(json) {
  console.log(json);
  
  localStorage.setItem('movies_json', JSON.stringify(json));
  return json
})
.catch(function(error) {
  console.log('error', error)
})
var url = 'http://localhost:3000/videogames.json'

fetch(url)
.then(function(response) {
  return response.json()
})
.then(function(json) {
  console.log(json);
  
  localStorage.setItem('videogames_json', JSON.stringify(json));
  return json
})
.catch(function(error) {
  console.log('error', error)
})
var url = 'http://localhost:3000/electronics.json'

fetch(url)
.then(function(response) {
  return response.json()
})
.then(function(json) {
  console.log(json);
  
  localStorage.setItem('electronics_json', JSON.stringify(json));
  return json
})
.catch(function(error) {
  console.log('error', error)
})
//localStorage.setItem('saved-store-state', JSON.stringify(productsData));
let store = createStore(
    rootReducer,
    {
        products: JSON.parse(localStorage.getItem('videogames_json')).items   // initial store values
    },
    compose(

      window.devToolsExtension ? window.devToolsExtension() : f => f)
);
if (window.devToolsExtension) window.devToolsExtension.updateStore(store)


render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);

